class TodosController < ApplicationController
  before_action :set_list

  def create
    @todo = Todo.new(params.require(:todo).permit(:description))
    @todo.list_id = @list.id
    if @todo.save
      redirect_to list_path(@list)
    else
      redirect_to list_path(@list), notice: @todo.errors.full_messages.to_sentence
    end
  end

  def flip_status
    @todo = Todo.find(params[:id])
    @todo.status = !@todo.status
    @todo.save
    redirect_to list_path(@list)
  end

  def destroy
    @todo = Todo.find(params[:id])
    @todo.destroy
    redirect_to list_path(@list)
  end

  private

  def set_list
    @list = List.find(params[:list_id])
  end
end
