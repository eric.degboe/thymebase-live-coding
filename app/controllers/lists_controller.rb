class ListsController < ApplicationController
  def index
    @lists = List.all
    @list = List.new
  end

  def show
    @list = List.find(params[:id])
    @todo = Todo.new
  end

  def create
    @list = List.new(params.require(:list).permit(:name))
    if @list.save
      redirect_to lists_path
    else
      @lists = List.all
      render :index
    end
  end

  def destroy
    
  end
end
