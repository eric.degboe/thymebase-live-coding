class Todo < ApplicationRecord
  belongs_to :list
  validates :description, presence: true
  validates :description, uniqueness: { scope: :list }
end
