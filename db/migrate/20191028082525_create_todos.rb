class CreateTodos < ActiveRecord::Migration[6.0]
  def change
    create_table :todos do |t|
      t.boolean :status
      t.text :description
      t.references :list, foreign_key: true

      t.timestamps
    end
  end
end
