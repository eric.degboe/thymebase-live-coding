require 'test_helper'

class ListTest < ActiveSupport::TestCase
  test "Saving  without name" do
    list = List.new
    refute list.save

    name_error = list.errors.messages[:name][0]

    assert_equal name_error, 'can\'t be blank'
  end
end
