Rails.application.routes.draw do
  get 'todos/create'
  resources :lists do
    resources :todos do
      member do
        get 'flip_status'
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
